# Changelog for Notifications portlet

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.6.0] - 2022-05-05

 - Added support for Catalogue notifications
 - Removed support for Tabular Data and Calendar Notifications

## [v2.5.0] - 2018-01-12

 - Minor update to the font style
 - Updated style and icons

## [v2.1.0] - 2017-02-22

 - Added notification settings support for process executions


## [v2.0.0] - 2016-06-23

 - Ported to Liferay 6.2
 - Responsive Design
 
## [v1.4.0] - 2015-07-03
 
 - Ported to GWT 270
 - Refactored with GWT Bootstrap

## [v1.2.0] - 2014-4-03

 - Implemented automatic notifications scroll back in time
 - Updated description for post alerts
 - Made Notification Settings more evident

## [v1.1.0] - 2013-10-21

 - Ported to GWT 251
 - Removed GCF Dependency
 - Logging with sl4j Enabled

## [v1.0.0] - 2013-01-13

First release


